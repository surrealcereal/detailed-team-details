from ruamel.yaml import YAML
import os

yaml = YAML()
yaml.default_flow_style = False
yaml.indent(mapping=4, sequence=4, offset=2)


def update_attributes_location(yaml_path, user_input):
    """
    Updates field "attribute_location" of yaml file with param user_input.

    :param str yaml_path: filepath to yaml file
    :param user_input: value to update entry with
    :return: None
    """
    with open(yaml_path, "r") as f:
        yaml_dict = yaml.load(f)
        yaml_dict["attributes_location"] = user_input
    with open(yaml_path, "w") as f:
        yaml.dump(yaml_dict, f)


def getoptions_main(yaml_path, csv_version):
    """
    Gets relevant information from config file and returns it.

    :param str yaml_path: filepath to config .yaml file
    :param csv_version: expected version to be present in config file
    :return: tuple containing relevant information
    :rtype: tuple
    """

    def install_dir_parser(filepath):
        """
        Replaces instances of "INSTALL_DIR" in filepaths with current working directory.

        :param str filepath: unparsed filepath
        :return: parsed filepath
        :rtype: str
        """
        try:
            return filepath.replace("INSTALL_DIR", os.getcwd())
        except AttributeError:
            return filepath

    def unchain_list_dict(list_dict):
        """
        Combines list of dictionaries into one dictionary.

        :param list list_dict: list of dictionaries
        :return: combined dictionary
        :rtype: dict
        """
        return {k: install_dir_parser(v) for d in list_dict for k, v in d.items()}
        # tuples and .update are not used for speed reasons

    with open(yaml_path, "r") as f:
        yaml_dict = yaml.load(f)
    ATTRIBUTES_LOCATION = install_dir_parser(yaml_dict["attributes_location"])
    images = unchain_list_dict(yaml_dict["images"])
    csv_mode = unchain_list_dict(yaml_dict["csv_mode"])

    if str(yaml_dict["version"]) != csv_version:
        raise RuntimeError(f"expected version from csv file is {csv_version} but got {yaml_dict['version']}")

    return ATTRIBUTES_LOCATION, images, csv_mode
