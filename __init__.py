import os
import subprocess
import time
from xml_parser import xmlparser_main
from get_options import getoptions_main, update_attributes_location
from ui import ui_main
from csv_mode import csv_main

# If you want to move your yaml file or rename it, change this variable to the new filepath, else your preferances will not be initialized and the script will crash.
CUSTOM_CONFIG_PATH = None

CSV_VERSION = "0.1"
DEFAULT_PATH = os.getcwd() + r"\config.yaml"

# initialize config
if CUSTOM_CONFIG_PATH is None:
    config_path = DEFAULT_PATH
else:
    if not os.path.exists(CUSTOM_CONFIG_PATH):
        raise ValueError(f"custom config path does not exist: {CUSTOM_CONFIG_PATH}")
    else:
        config_path = CUSTOM_CONFIG_PATH

ATTRIBUTES_LOCATION, images, csv_dict = getoptions_main(config_path, CSV_VERSION)

# ask for filepath of attributes.xml on first execution
if ATTRIBUTES_LOCATION is None:
    # open in new window to avoid --noconsole
    subprocess.Popen(["start", r"misc\get_attributes_location.exe"], creationflags=subprocess.CREATE_NEW_CONSOLE, shell=True)
    temp_file = os.path.expandvars(r"%TEMP%\detailedHuntDetails.tmp")
    while not os.path.isfile(temp_file):  # wait for user to input this way as .wait() doesn't work with "start"
        time.sleep(1)
    with open(temp_file, "r") as f:
        user_input = f.read()
    os.remove(temp_file)
    update_attributes_location(DEFAULT_PATH, user_input)
    ATTRIBUTES_LOCATION = user_input

# start parsing
match = xmlparser_main(ATTRIBUTES_LOCATION)

# CSV mode
if csv_dict["is_on"]:
    csv_main(csv_dict["target"], match)
    print(f"Successfully created .csv file at {csv_dict['target']}")
else:
    # UI mode
    ui_main(match, images, ATTRIBUTES_LOCATION)
