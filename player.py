class Player:
    """
    Class containing info about a player and their relationship to the user.
    """

    def __init__(self, player_no):
        """
        Initializes Player instance with None default values and player_no as player number.

        :param int player_no: number of player
        """
        self.player_no = player_no
        self.name = None
        self.had_wellspring = None
        self.had_bounty = None
        self.killed_by_me = None
        self.killed_me = None
        self.mmr = None
        self.profile_id = None
        self.used_voip = None
        self.had_skillbased_on = None

    def populate(self, info, value):
        """
        Populates attribute param info of Player instance with param value.

        :param str info: name of attribute
        :param bool or str value: value to populate attribute with
        :return: None
        """
        setattr(self, info, value)


class Team:
    """
    Class containing info about a team, consisting of Player instances.
    """

    def __init__(self, team_no):
        """
        Initializes Team instance with None default values, team_no as number of team and a list to contain Player instances.

        :param int team_no: number of team
        """
        self.players = list()
        self.team_no = team_no
        self.handicap = None
        self.is_invite = None
        self.mmr = None
        self.player_count = None
        self.is_own_team = None

    def populate(self, info, value):
        """
        Populates attribute param info of Team instance with param value.

        :param str info: name of attribute
        :param bool or str value: value to populate attribute with
        :return: None
        """
        setattr(self, info, value)

    def add_player(self, player_no):
        """
        Adds player with param player_no to team if player count is not exceeded.

        :param int player_no: number of player
        :raises ValueError: if player count is exceeded
        :return: None
        """
        if len(self.players) < self.player_count:  # skip over erroneous entries remaining from prior matches
            self.players.append(Player(player_no))

    def is_valid_player(self, player_no):
        """
        Checks if player with param player_no exists.

        :param int player_no: number of player
        :rtype bool:
        """
        try:
            self.players[player_no]
        except IndexError:
            return False
        return True


class Match:
    """
    Class containing Team instances, representing a match of Hunt: Showdown.
    """

    def __init__(self):
        """
        Initializes Match instance with type of match, referring to Quickplay or Bounty Hunt, and a list to contain Team instances.
        """
        self.teams = list()
        self.type = None
        self.team_count = None
        self.total_player_count = None

    def set_type(self, is_quickplay):
        """
        Sets type of match to either 'quickplay' or 'bounty'.

        :param bool is_quickplay: whether the type of match is quickplay or not, retrieved from xml file
        :return: None
        """
        if is_quickplay:
            self.type = "quickplay"
        else:
            self.type = "bounty"

    def add_team(self, team_no):
        """
        Adds team with param team_no to Match instance if the team already doesn't exist.

        :param int team_no: number of team
        :return: None
        """
        if not self.is_valid_team(team_no):
            self.teams.append(Team(team_no))

    def add_player_to_team(self, team_no, player_no):
        """
        Adds player to team if player already doesn't exist.

        Note that this method will do nothing if both the team and the player already exist.

        :param int team_no: number of team
        :param int player_no: number of player
        :return: None
        """
        if not self.teams[team_no].is_valid_player(player_no):
            self.teams[team_no].add_player(player_no)

    def is_valid_team(self, team_no):
        """
        Checks if team with param team_no already exists.

        :param team_no: number of team
        :return: True if team exists, False if not
        :rtype: bool
        """
        try:
            self.teams[team_no]
        except IndexError:
            return False
        return True

    def rearrange_teams(self):
        """
        Rearranges team structure so that Team 0 is always the user's team.

        :return: None
        """
        for i, team in enumerate(self.teams):
            if team.is_own_team:
                team_0 = self.teams[0]
                self.teams[0] = team
                self.teams[i] = team_0
                return

