import csv
import os


def csv_main(filepath, match):
    """
    Creates .csv file from information contained within player.Match instance.

    :param str filepath: filepath to .csv file to be created
    :param player.Match match: player.Match instance
    :raises ValueError: if param filepath does not point to valid filepath
    :return: None
    """
    if filepath is None or not os.path.exists(filepath):
        raise ValueError(f"not a valid path: {filepath}")
    with open(filepath, mode="w", encoding="utf-8-sig") as file:
        fieldnames = ["team_no", "player_no", "name", "had_wellspring", "had_bounty", "killed_by_me", "killed_me",
                      "mmr", "profile_id",
                      "used_voip", "had_skillbased_on"]
        writer = csv.writer(file)
        writer.writerow(fieldnames)
        row = list()
        for team in match.teams:
            for player in team.players:
                row.append(team.team_no)
                for i in fieldnames[1:]:
                    row.append(getattr(player, i))
                writer.writerow(row)
                row.clear()
