import os.path
from ctypes import windll, byref, create_unicode_buffer


def add_font(fontpath):
    """Makes fonts located on the filepath param fontpath available to the font system.

    :param str or bytes fontpath: filepath to font file
    :raises ValueError: if param filepath does not point to valid font file
    :return: None
    """
    # Taken from: https://github.com/ifwe/digsby/blob/f5fe00244744aa131e07f09348d10563f3d8fa99/digsby/src/gui/native/win/winfonts.py#L15
    if not os.path.isfile(fontpath) or not fontpath.endswith(".ttf"):
        raise ValueError(f"invalid font file: {fontpath}")
    pathbuf = create_unicode_buffer(fontpath)
    AddFontResourceEx = windll.gdi32.AddFontResourceExW
    AddFontResourceEx(byref(pathbuf), 0x10, 0)
