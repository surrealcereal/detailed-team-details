# Detailed Team Details 
[![python310](https://img.shields.io/badge/python-v3.10-blue)](https://github.com/surrealcereal/detailed-team-details) [![windows](https://shields.io/badge/platform-windows-lightgrey)](https://github.com/surrealcereal/detailed-team-details) 

**Detailed Team Details** is a utility meant for Hunt: Showdown, used to display a more detailed Team Details screen, using information as present in the `attributes.xml` file, located at `steam_directory\steamapps\common\Hunt Showdown\user\profiles\default\attributes.xml`.

## Notes on the UI
In order to reveal the exact MMR a player has, click on the picture of their matchmaking rating (stars). Additionally, the 'killed' and 'died to' skulls can be clicked on to reveal exactly how many times you have killed or died to that player. These are toggleable, so clicking on them again will revert them to their original state (and vice versa).

## Antivirus Hits
This utility is not malware. Some antivirus solutions may detect it as such, since the program is frozen with PyInstaller, which may also be used by cybercriminals to ship malicious code. If you are unsure, you can always either freeze the application yourself or use the source code with your own Python interpreter (which must be at least version 3.10).

## First Execution
As the script does not know where your `attributes.xml` file is located, a command line window will appear, only for your first time executing, and prompt you for its location. 

## Config File
With every release and in the repository, a `config.yaml` file is present, which can be used to re- and further customize the behaviour of the utility, using [YAML](https://yaml.org/spec/1.2.2/#chapter-2-language-overview) syntax. In this file you can customize:
* the location of your `attributes.xml` file,
* the location of the image files used by the UI,
* and whether to display the data in a GUI window (default) or to dump them into a csv file

### Dumping into csv
You can enable csv dumping by setting `is_on: true` in your config file and specifying a filepath for `target`. Do note that the `target` must include the resulting filename, as in `directory\file.csv`.

### Custom Config Location
The releases do not support custom config file locations, and a config file named `config.yaml` must be present alongside your main `DetailedTeamDetails.exe` executable. 

However, by using the source with your own Python interpreter (which must be at least version 3.10) you can change the `CUSTOM_CONFIG_PATH` variable in `__init__.py` to a filepath which contains the file.

## Building from Source
The Python code is frozen using the latest version of [PyInstaller](https://pypi.org/project/pyinstaller/), therefore you must do `pip install pyinstaller` if you do not have it installed already.

Note that the commands provided assume that the current working directory contains the relevant resources. Modify the filepaths in the commands if this is not the case.

The releases are frozen using the following command:
```
pyinstaller --onefile __init__.py --noconsole --paths=%cd% --icon=misc\icon.ico -n DetailedTeamDetails.exe
```

This project also makes use of very minimal C++, in the case of getting the filepath to `attributes.xml` for the first execution. The source code for this C++ applet is in `get_attributes_location.cpp`, which must be compiled for the utility to function.

In the releases, the compilation is achieved through [MinGW](https://sourceforge.net/projects/mingw-w64/), using the following command:
```
g++ -O2 -o misc\get_attributes_location.exe -static get_attributes_location.cpp
```

After these steps, you can execute `DetailedTeamDetails.exe` as normal and run the utility.