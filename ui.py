from datetime import datetime
import os
from add_font import add_font
from tkinter import Label, Button, Tk
from PIL import ImageTk, Image


class ImageButton(Button):
    """
    Extended tk.Button optimized against repetitve code for buttons that change between images and text on click.

    Note that any instance of this class automatically initializes with root as self.master.
    """

    def __init__(self, tag="image", **kwargs):
        """
        Initializes ImageButton instance with an image displaying as default.

        :param str tag: either "image" or "text", signifying the state of the button
        """
        Button.__init__(self, root, **kwargs)
        self.tag = tag
        self.config(borderwidth=0)
        self.config(bg="#101114", fg="#a7abad", activebackground="#101114")


class ExtendedLabel(Label):  # always init in root
    """
    Extended tk.Label optimized for labels with a bicolor color scheme to avoid repetitive code.

    Note that any instance of this class automatically initializes with root as self.master.
    """

    def __init__(self, is_background=False, **kwargs):
        """
        Initializes Extended_Label instance with the foreground color as default.

        :param bool is_background: whether the label has the foreground or background color
        """
        Label.__init__(self, root, **kwargs)
        self.is_background = is_background
        self.config(fg="#a7abad")
        if is_background:
            self.config(bg="#2b2d32")
        else:
            self.config(bg="#101114")


def get_image(image_filepath):
    """
    Converts filepath to ImageTk.PhotoImage instance interpretable by tkinter.

    :param str image_filepath: filepath to image
    :return: initialized ImageTk instance
    :rtype: PIL.ImageTk
    """
    return ImageTk.PhotoImage(Image.open(image_filepath))


def get_stars(mmr):
    """
    Converts param MMR value to the corresponding MMR bracket.

    :param int mmr: MMR value
    :return: PIL.ImageTk instance of MMR image
    :rtype: PIL.ImageTk
    """
    match mmr:
        case mmr if 0 <= mmr < 2000:
            return images["1_star"]
        case mmr if 2000 <= mmr < 2300:
            return images["2_stars"]
        case mmr if 2300 <= mmr < 2600:
            return images["3_stars"]
        case mmr if 2600 <= mmr < 2750:
            return images["4_stars"]
        case mmr if 2750 <= mmr < 3000:
            return images["5_stars"]
        case mmr if 3000 <= mmr:
            return images["6_stars"]
        case _:
            return images["0_stars"]


def toggle(button, imagetk_instance, text):
    """
    Toggles between image and text for ImageButton instances.

    :param ImageButton button: ImageButton instance to be toggled
    :param PIL.ImageTk imagetk_instance: ImageTk instance of image
    :param str text: text to be displayed
    :return: None
    """
    global _im
    _im = imagetk_instance
    if button.tag == "text":  # switch to image
        button.config(image=_im, text="")  # get_stars(player.mmr)
        button.tag = "image"
    elif button.tag == "image":  # switch to text
        button.config(image="", text=text)
        button.tag = "text"


def create_data_row(player):
    """
    Creates row in UI and populates it with data from player.Player instance.

    :param player.Player player: player.Player instance
    :return: None
    """
    global current_row
    if len(player.name) > 15:
        player.name = player.name[:15] + "..."
    name = ExtendedLabel(text=player.name)
    had_objective = ExtendedLabel(image=images["had_objective"] if player.had_bounty or player.had_wellspring else "")
    used_voip = ExtendedLabel(image=images["used_voip"] if player.used_voip else "")

    mmr = ImageButton(command=lambda: toggle(mmr, get_stars(player.mmr), player.mmr), image=get_stars(player.mmr))

    if player.killed_by_me > 0:
        killed_by_me = ImageButton(command=lambda: toggle(killed_by_me, images["killed_by_me"], player.killed_by_me),
                                   image=images["killed_by_me"])
    else:
        killed_by_me = ExtendedLabel()

    if player.killed_me:
        killed_me = ImageButton(command=lambda: toggle(killed_me, images["killed_me"], player.killed_me),
                                image=images["killed_me"])
    else:
        killed_me = ExtendedLabel()

    horizontal_spaces = list()
    for i in range(6):
        horizontal_spaces.append(ExtendedLabel(is_background=True))

    setup = [name, horizontal_spaces[0], mmr, horizontal_spaces[1], had_objective, horizontal_spaces[2],
             killed_by_me,
             horizontal_spaces[3], killed_me, horizontal_spaces[4], used_voip]

    weights = {
        name: 50,
        mmr: 35,

        had_objective: 10,
        killed_by_me: 10,
        killed_me: 10,
        used_voip: 10,

        horizontal_spaces[0]: 1,
        horizontal_spaces[1]: 1,
        horizontal_spaces[2]: 1,
        horizontal_spaces[3]: 1,
        horizontal_spaces[4]: 1,
        horizontal_spaces[5]: 1
    }

    for i, box in enumerate(setup):
        box.grid(column=i, row=current_row, sticky="nsew")
        root.grid_columnconfigure(i, weight=weights[box], uniform="group1")
    root.grid_rowconfigure(current_row, weight=100, uniform="group1")
    current_row += 1


def create_space_row(row_type):
    """
    Creates row to be used as visual space in UI.

    :param str row_type: type of space, either 'interplayers', 'interteams' or 'beginning'
    :return: None
    """
    global current_row
    space = ExtendedLabel(is_background=True)

    weights = {
        "interplayers": 4,  # originally 5
        "interteams": 20,
        "beginning": 40
    }

    space.grid(column=0, row=current_row, sticky="nsew", columnspan=12)
    root.grid_rowconfigure(current_row, weight=weights[row_type], uniform="group1")  # TODO: look at minsize
    current_row += 1


def create_team_name_row(team):
    """
    Creates row in UI and populates it with name of the team proceeding it.

    :param player.Team team: player.Team instance
    :return: None
    """
    global current_row
    team_name = ExtendedLabel(is_background=True, text="Your Team" if team.is_own_team else f"Team {team.team_no}")
    space = ExtendedLabel(is_background=True)
    team_name.grid(column=0, row=current_row, sticky="nsew")
    space.grid(column=1, row=current_row, sticky="nsew", columnspan=11)
    root.grid_rowconfigure(current_row, weight=100, uniform="group1")
    current_row += 1


def ui_main(match, _images, attributes_location):
    """
    Creates UI window using player.Match instance.

    :param player.Match match: player.Match instance
    :param dict _images: dictionary containing filepaths to image files
    :param str attributes_location: filepath of attributes.xml
    :return: None
    """

    def get_file_date(filepath):
        """
        Returns modification date for file at param filepath.

        :param str filepath: filepath to file
        :return: modification date
        :rtype: str
        """
        return datetime.utcfromtimestamp(os.path.getmtime(filepath)).strftime('%H:%M:%S %Y-%m-%d')

    global root, images, current_row
    images = _images
    root = Tk()
    root.geometry("610x700")
    root.title(f"Hunt: Showdown Match at {get_file_date(attributes_location)}")
    root.iconbitmap(os.getcwd() + r"\misc\icon.ico")
    add_font(os.getcwd() + r"\misc\DroidSerif-Bold.ttf")
    root.option_add('*Font', ('Droid Serif', 12))
    for k, v in images.items():  # initialized images need to be in global namespace to avoid garbage collection
        images[k] = get_image(v)

    # main loop
    match.rearrange_teams()
    current_row = 0
    for team in match.teams:
        create_team_name_row(team)
        for player in team.players:
            create_data_row(player)
            if not player.player_no == team.player_count - 1:
                create_space_row("interplayers")
    root.mainloop()
