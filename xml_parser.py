from player import Match
import xml.etree.ElementTree as xml


def evaluate_string(string):
    """
    Evaluates string booleans to boolean values and string ints to int values.

    Returns string param unchanged if it is not equal to 'true' or 'false' and is not valid int.

    :param str string: bool in string form
    :return: bool value if valid bool string param, int value if valid int string param, else string param
    :rtype: bool or int or str
    """
    try:
        return int(string)
    except ValueError:
        pass
    if string == "true":
        return True
    elif string == "false":
        return False
    else:
        return string


def get_equivalent(xml_entry):
    """
    Converts the language used in the xml file to valid attribute names to be used in population of player.Player or player.Team instances.

    :param str xml_entry: entry in language used by the xml file
    :return: valid player.Player or player.Team instance attribute
    :raises ValueError: if xml_entry param is not found in the recognized entries list
    """
    equivalent_dict = {
        # Player instance attributes and xml equivalents
        "blood_line_name": "name",
        "hadWellspring": "had_wellspring",
        "hadbounty": "had_bounty",
        "killedbyme": "killed_by_me",
        "killedme": "killed_me",
        "mmr": "mmr",
        "profileid": "profile_id",
        "proximity": "used_voip",
        "skillbased": "had_skillbased_on",

        # Team instance attributes and xml equivalents
        "handicap": "handicap",
        "isinvite": "is_invite",
        "numplayers": "player_count",
        "ownteam": "is_own_team"
    }
    try:
        return equivalent_dict[xml_entry]
    except KeyError:
        raise ValueError(f"Unrecognized xml_entry: '{xml_entry}'")


def get_xml_dict():
    """
    Converts xml file into dict.

    :return: xml file in dictionary format
    :rtype: dict
    """
    tree = xml.parse(ATTRIBUTES_LOCATION)
    root = tree.getroot()
    xml_dict = dict()
    for x in root.findall(".//"):
        xml_dict[x.attrib["name"]] = x.attrib["value"]
    return xml_dict


def mission_bag(xml_dict):
    """
    Gets MissionBag info from xml_dict, sets various match information, creates teams and parses MissionBagPlayer entries from MissionBag.

    :param dict xml_dict: xml file in dictionary format
    :return: None
    """
    mission_bag = {k: v for k, v in xml_dict.items() if k.startswith("MissionBag")}
    match.set_type(evaluate_string(mission_bag["MissionBagIsQuickPlay"]))
    match.team_count = int(mission_bag["MissionBagNumTeams"])  # no method used as no parsing necessary
    for i in range(match.team_count):
        match.add_team(i)


def mission_bag_teams(xml_dict):
    """
    Gets MissionBag info from xml_dict, parses MissionBagTeam entries from it and creates teams.

    :param dict xml_dict: xml file in dictionary format
    :return: None
    """
    teams = {k: v for k, v in xml_dict.items() if k.startswith("MissionBagTeam")}
    for k, v in teams.items():
        try:
            team_no, info = k.lstrip("MissionBagTeam_").split(
                "_")
        except ValueError:
            continue  # Do nothing if the attr is just MissionBagTeam_x = 1
        team_no = int(team_no)
        if team_no <= match.team_count - 1:  # team_no starts from 0
            match.teams[team_no].populate(get_equivalent(info), evaluate_string(v))
        # do not else break, as 10 is after 0 in xml file


def init_match(xml_dict):
    """
    Populates player.Match instance with information about the players, as present in MissionBagPlayer entries.

    :param dict xml_dict: xml file in dictionary format
    :return: None
    """
    players = {k: v for k, v in xml_dict.items() if k.startswith("MissionBagPlayer")}
    for k, v in players.items():
        team_no, player_no, *info = k.lstrip("MissionBagPlayer_").split(
            "_")  # *info as the title of the value may contain underscores itself
        info = "_".join(info)
        team_no = int(team_no)
        player_no = int(player_no)
        if team_no <= match.team_count - 1:  # team no starts from 0
            match.add_player_to_team(team_no, player_no)
            try:
                match.teams[team_no].players[player_no].populate(get_equivalent(info), evaluate_string(v))
            except IndexError:  # skip over erroneous entries remaining from prior matches
                pass


def xmlparser_main(_attributes_location):
    """
    Converts MissionBag data from xml file into player.Match instance.

    :param str _attributes_location: filepath of attributes.xml
    :return: populated player.Match instance
    :rtype: player.Match
    """
    global ATTRIBUTES_LOCATION, match
    ATTRIBUTES_LOCATION = _attributes_location
    match = Match()
    xml_dict = get_xml_dict()
    mission_bag(xml_dict)
    mission_bag_teams(xml_dict)
    init_match(xml_dict)
    return match
