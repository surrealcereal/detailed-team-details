#include <fstream>
#include <iostream>
#include <Windows.h>

using namespace std;

string extendTemp(string filepath){
    string strTempPath;
    char wchPath[MAX_PATH];
    if (GetTempPathA(MAX_PATH, wchPath))
        strTempPath = wchPath;
    strTempPath += "\\";
    return strTempPath + filepath;
}

bool fileExists(string filepath){
    ifstream ifile;
    ifile.open(filepath);
    if (ifile) {
        ifile.close();
        return true;
    }
    ifile.close();
    return false;
}

bool hasEnding (string const &fullString, string const &ending) {
    if (fullString.length() >= ending.length()) {
        return (0 == fullString.compare (fullString.length() - ending.length(), ending.length(), ending));
    } else {
        return false;
    }
}

int main(){
    string userInput;
    cout << "Please provide filepath to your attributes.xml file (asked only on first execution): ";
    getline(cin, userInput);
    while (!fileExists(userInput) || !hasEnding(userInput, "attributes.xml")){
        system("cls");
        cout << userInput << " is not a valid path or does not point to a file named attributes.xml. Please provide a valid filepath." << endl;
        cout << "Please provide filepath to your attributes.xml file: ";
        getline(cin, userInput);
    }
    // I am aware that this could be handled through named pipes or polling stdout for the last output, however that'd take more effort than it's worth.
    ofstream tempfile;
    tempfile.open(extendTemp("detailedHuntDetails.tmp"));
    tempfile << userInput;
    tempfile.close();
    return 0;
}
